# SnowBall QuoteApp

    Personal Project

## Installation

    You need Nodejs version 18 or higher 

    follow these steps:
        1.npm install
        2.npm run dev

## About 

    I've recently built a simple Quote Application using Vue 3, Pinia, and the Composition API.
    This project was undertaken purely for my personal learning journey.
    It allowed me to explore Vue 3's latest features, understand how to use Pinia for state management, and gain hands-on experience with the Composition API.
    Through this project, I aimed to deepen my knowledge of Vue.js while building a practical and functional application for managing and displaying quotes.

    In the project's source code, I've added extensive comments for reference and learning purposes. These comments
    serve as valuable insights into the code's functionality, explaining how different parts of the application work.
    They are intended to aid my own understanding and can be a helpful resource for others looking to learn Vue.js,
    Pinia, or the Composition API through this project.
   
    Please note that this project is not connected to a backend or a database. It's a frontend-focused application
    designed solely for the purpose of learning and practicing Vue.js and related technologies.


